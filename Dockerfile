FROM nginx:1.15-alpine

COPY market-place/build /usr/share/nginx/flow

RUN ls /usr/share/nginx

COPY nginx.conf /etc/nginx/nginx.conf

RUN adduser mosaic --disabled-password

RUN chown -Rv mosaic /usr/share/nginx/flow

USER mosaic

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]

