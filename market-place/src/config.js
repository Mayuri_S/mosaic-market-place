const host = "http://52.187.49.85:8084";
export const config = {
  urls: {
    FETCH_SIDEBAR_ITEMS: host + "/MarketPlace/categoryTypes/getAll",
    FETCH_CATEGORIES: host + "/MarketPlace/category/v2/getCategoriesByTypeId/",
    FETCH_SOLUTIONS:
      host + "/MarketPlace/solution/v2/getSolutionsByCategoryId/",
  },
};
