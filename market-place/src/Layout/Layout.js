import React, { Component } from "react";
import SideBar from "../component/SideBar";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import { config } from "../config";
import Solutions from "../component/Solutions";
import Category from "../component/Category";
import Header from "../component/Header";
import Industry from "../component/Industry";

export class Layout extends Component {
  state = {
    sideNavItems: [],
    categories: [],
    breadcrumb: [{ label: "Accelerators", link: null, isActive: false }],
    typeName: "",
  };
  componentDidMount() {
    // Fetch side navbar items
    axios
      .get(config.urls.FETCH_SIDEBAR_ITEMS)
      .then((response) => {
        if (response && response.data) {
          const navItems = response.data.data;
          this.setState({
            ...this.state,
            sideNavItems: navItems,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
    // Fetch Default Caegory
    this.fetchCategory(
      sessionStorage.getItem("categoryId")
        ? sessionStorage.getItem("categoryId")
        : "58da01c1c5d4e31d8cb5bfff"
    );
  }

  fetchCategory = (id) => {
    axios
      .get(config.urls.FETCH_CATEGORIES + id)
      .then((response) => {
        if (response && response.data) {
          const categories = response.data.data;
          const typeName = response.data.other
            ? response.data.other.typeName
            : "";
          sessionStorage.setItem("category", typeName);
          sessionStorage.setItem("categoryId", id);
          const breadcrumb = [{ label: typeName, link: null, isActive: false }];
          this.updateBreadcrumb(breadcrumb);
          this.setState({
            ...this.state,
            categories: categories,
            typeName: typeName,
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  updateBreadcrumb = (data) => {
    this.setState({
      ...this.state,
      breadcrumb: data,
    });
  };

  render() {
    const { sideNavItems, categories, breadcrumb, typeName } = this.state;
    const categoryId = this.props.match.params.id;
    return (
      <React.Fragment>
        <Header breadcrumb={breadcrumb} />
        <Grid justify="left" container spacing={1}>
          <Grid item xs={1}>
            <SideBar
              navItems={sideNavItems}
              fetchCategory={this.fetchCategory}
            />
          </Grid>
          <Grid item xs={11}>
            {categoryId ? (
              <Solutions
                categoryId={categoryId}
                updateBreadCrumb={this.updateBreadcrumb}
              />
            ) : (
              <Category categories={categories} typeName={typeName} />
            )}
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

export default Layout;
