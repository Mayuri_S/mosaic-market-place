import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import { Link, useRouteMatch } from "react-router-dom";

const StyledSideNav = {
  position:
    "fixed" /* Fixed Sidebar (stay in place on scroll and position relative to viewport) */,
  height: "70%",
  width: "70px",
  borderTopRightRadius: "20px",
  borderBottomRightRadius: "20px",
  paddingTop: "100px" /* Set the width of the sidebar */,
  /* Stay on top of everything */
  top: "2.6rem" /* Stay at the top */,
  backgroundColor: "#222" /* Black */,
  /* Disable horizontal scroll */
};

export default class SideBar extends Component {
  render() {
    var name = "";
    const { navItems } = this.props;
    const activeNav = sessionStorage.getItem("category");
    return (
      <div style={StyledSideNav}>
        {navItems.map((item, index) => (
          // {require("../../../assets/images/" + {item.typeName} + ".png")}
          <Link to={"/"} style={{ textDecoration: "none" }}>
            <Grid
              key={index}
              className={
                activeNav === item.typeName
                  ? ["itemStyle", "activeNav"].join(" ")
                  : "itemStyle"
              }
              onClick={() => {
                this.props.fetchCategory(item.id);
              }}
            >
              <img
                src={require("../assets/images/" + item.typeName + ".png")}
                style={{
                  paddingLeft: "15px",
                  paddingTop: 8,
                  alignItems: "center",
                }}
                width={30}
                height={22}
              />
              <Typography
                style={{
                  color: "white",
                  textAlign: "center",
                }}
              >
                <span style={{ fontSize: "10px" }}>{item.typeName}</span>
              </Typography>
            </Grid>
          </Link>
        ))}
      </div>
    );
  }
}
