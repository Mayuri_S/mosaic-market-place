import React, { Component } from "react";
import SideBar from "./SideBar";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import logo from "../logo.svg";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

import { Typography } from "@material-ui/core";

const Styles = {
  textAlign: "center",
  color: "black",
};
export default class Industry extends Component {
  state = {
    isDrawerOpen: [],
    toggle: true,
  };
  visibility = (event, currentIndex) => {
    let defaultValue = true;
    let toggle = !defaultValue;
    let showEdit = this.state.isDrawerOpen.map((val, index) => {
      return (val = false);
    });
    let isDrawerOpen = showEdit.slice();

    isDrawerOpen[currentIndex] = this.state.toggle;
    this.setState({ isDrawerOpen, toggle: !this.state.toggle });
  };
  render() {
    let processing = [
      "Deduplication",
      "Aggregated data",
      "Matched and merged profiles",
    ];
    let outcomes = [
      "Accounts Receivable Trend, Impact & other KPIs",
      "Accounts Receivable Executive",
      "Finance Manager Sales Manager",
      "Production Manager",
    ];
    let inputDataSources = [
      "Outlets Details like Name, Address, and Phone Number etc.",
      "External data from Map My India, Google Places, Retail Scan, Google Lepton",
    ];

    return (
      <>
        {/* <SideBar></SideBar> */}
        <Box style={{ marginTop: "3.4em" }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Grid container justify="center" spacing={4}>
                {[0, 1, 2, 3, 4, 5, 6].map((value, index) => (
                  <>
                    <Grid key={value} item xs={3}>
                      <Paper
                        style={Styles}
                        spacing={1}
                        onClick={(e) => this.visibility(e, index)}
                      >
                        <div>
                          <img src={logo} alt="description of image"></img>
                          <br></br>
                          <div style={{ color: "blue" }}>Industry</div>
                        </div>
                      </Paper>
                    </Grid>
                    {this.state.isDrawerOpen[index] ? (
                      <div className={"tooltip"}>
                        <Grid
                          container
                          direction="row"
                          justify="center"
                          style={{ height: "auto", paddingBottom: "30px" }}
                        >
                          <Grid className={"toolHeaderBox"}>
                            <Typography
                              gutterBottom
                              variant="h5"
                              className={"tooltipHeader"}
                            >
                              Customer Centricity
                            </Typography>
                            <Typography
                              variant="p"
                              className={"tooltipSubHeader"}
                            >
                              Retail Company sell products at multiple outlets
                              and needs to have a complete view of their
                              outlets. So the company wanted to enrich the
                              existing data through match and merge with
                              external third party data providers.",
                            </Typography>
                          </Grid>
                          <div
                            style={{
                              width: "300px",
                              border: "1px solid grey",
                              background: "white",
                            }}
                          ></div>
                        </Grid>

                        <Grid
                          container
                          direction="row"
                          style={{ paddingTop: "40px", height: "auto" }}
                        >
                          <Grid
                            container
                            direction="row"
                            style={{ width: "300px", paddingRight: "55px" }}
                          >
                            <img
                              src={Industry}
                              style={{
                                backgroundColor: "black",
                                height: "47px",
                                width: "44px",
                              }}
                            ></img>
                            <Typography
                              gutterBottom
                              variant="h6"
                              className={"inputDataSource"}
                            >
                              Input Data Sources
                            </Typography>
                            <ul style={{}}>
                              {inputDataSources.map(
                                (inputDataSource, index) => (
                                  <li
                                    key={index}
                                    style={{ lineHeight: "24px" }}
                                    className={"tooltipSubHeader"}
                                  >
                                    {inputDataSource}
                                  </li>
                                )
                              )}
                            </ul>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            style={{ width: "300px", paddingRight: "55px" }}
                          >
                            <img
                              src={Industry}
                              style={{
                                backgroundColor: "black",
                                height: "47px",
                                width: "44px",
                              }}
                            ></img>
                            <Typography
                              gutterBottom
                              variant="h6"
                              className={"inputDataSource"}
                            >
                              Processing
                            </Typography>
                            <ul style={{}}>
                              {processing.map((process, index) => (
                                <li
                                  key={index}
                                  style={{ lineHeight: "24px" }}
                                  className={"tooltipSubHeader"}
                                >
                                  {process}
                                </li>
                              ))}
                            </ul>
                          </Grid>
                          <Grid
                            container
                            direction="row"
                            style={{ width: "300px", paddingRight: "55px" }}
                          >
                            <img
                              src={Industry}
                              style={{
                                backgroundColor: "black",
                                height: "47px",
                                width: "44px",
                              }}
                            ></img>
                            <Typography
                              gutterBottom
                              variant="h6"
                              className={"inputDataSource"}
                            >
                              Outcomes
                            </Typography>
                            <ul style={{}}>
                              {outcomes.map((outcome, index) => (
                                <li
                                  key={index}
                                  style={{ lineHeight: "24px" }}
                                  className={"tooltipSubHeader"}
                                >
                                  {outcome}
                                </li>
                              ))}
                            </ul>
                          </Grid>
                        </Grid>
                        <Grid
                          container
                          direction="row"
                          style={{ paddingTop: "40px", height: "auto" }}
                        >
                          <Typography
                            variant="body2"
                            color="#666"
                            component="p"
                            className={"footerText"}
                          >
                            By:
                          </Typography>
                          <img
                            src={logo}
                            alt="logo"
                            style={{ width: "24px", height: "24px" }}
                          ></img>
                          <Typography
                            variant="body2"
                            color="#666"
                            component="p"
                            className={"footerText"}
                          >
                            MOSAIC <sup>TM</sup> Decisions
                          </Typography>
                          <Grid
                            container
                            direction="row"
                            justify="flex-end"
                            style={{ marginTop: "-3rem" }}
                          >
                            {" "}
                            <Button
                              variant="outlined"
                              color="#17b9e7"
                              className={"buttonDesign"}
                              style={{ minHeight: "130px", maxHeight: "130px" }}
                            >
                              I am interested
                            </Button>{" "}
                            <Button
                              variant="outlined"
                              color="#17b9e7"
                              className={"buttonDesign"}
                            >
                              Launch
                            </Button>
                          </Grid>
                        </Grid>
                      </div>
                    ) : null}
                  </>
                ))}
              </Grid>
            </Grid>
          </Grid>
        </Box>
      </>
    );
  }
}
