import React, { Component } from "react";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import SolutionDetails from "../component/SolutionDetails";
import ReactDOM from "react-dom";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import { config } from "../config";
import _ from "lodash";

const Styles = {
  textAlign: "center",
  color: "black",
  height: "200px",
  width: "270px",
  cursor: "pointer",
  margin: 8,
};

const Styles_After = {
  textAlign: "center",
  color: "black",
  height: "200px",
  width: "270px",
  cursor: "pointer",
  margin: 8,
  border: "1.5px solid #17b9e7",
};
const popoverStyle = {
  width: "90%",
};

const visibleDiv = [];
export default class Solutions extends Component {
  state = {
    tooltip: false,
    solutions: [],
    solution: {},
    headerData: {},
  };
  constructor() {
    super();
    this.toolConst = false;
  }

  componentDidMount() {
    const categoryId = this.props.categoryId;
    const categoryName = sessionStorage.getItem("category");
    axios
      .get(config.urls.FETCH_SOLUTIONS + categoryId)
      .then((response) => {
        if (response && response.data) {
          const solutions = response.data.data;
          const subCategory = response.data.other
            ? response.data.other.categoryName
            : "";
          const breadcrumb = [
            { label: categoryName, link: "/", isActive: false },
            { label: subCategory, link: null, isActive: false },
          ];
          const headerData = response.data.other;
          this.setState({
            ...this.state,
            solutions: solutions,
            headerData: headerData,
          });
          this.updateBreadcrumb(breadcrumb);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  updateBreadcrumb = (data) => {
    this.props.updateBreadCrumb(data);
  };

  visibility = (event, solution, index) => {
    let renderFlag = true;
    const length = this.state.solutions.length;
    let position = Math.ceil((index + 1) / 4) * 4;
    if (document.getElementById("solution-" + position) === null) {
      position = length;
    }
    const element = document.getElementById("solution-" + position) ? (
      document.getElementById("solution-" + position)
    ) : (
      <div></div>
    );
    if (!_.includes(visibleDiv, "solution-" + position)) {
      _.each(visibleDiv, (div) => {
        const divToRemove = document.getElementById(div);
        if (divToRemove) {
          ReactDOM.render(<div id={div} />, divToRemove);
          _.remove(visibleDiv, (data) => {
            return data === div;
          });
          this.setState({
            solution: "",
          });
        }
      });
    } else {
      if (
        visibleDiv.length > 0 &&
        solution.solutionName === this.state.solution.solutionName
      ) {
        _.each(visibleDiv, (div) => {
          const divToRemove = document.getElementById(div);
          if (divToRemove) {
            ReactDOM.render(<div id={div} />, divToRemove);
            _.remove(visibleDiv, (data) => {
              return data === div;
            });
            this.setState({
              solution: "",
            });
            renderFlag = false;
          }
        });
      }
    }
    if (renderFlag) {
      ReactDOM.render(<SolutionDetails solution={solution} />, element);
      visibleDiv.push("solution-" + position);
      this.setState({
        tooltip: !this.state.tooltip,
        solution: solution,
      });
    }
  };

  render() {
    const { solutions, solution, headerData } = this.state;
    return (
      <div className={"solution-card"}>
        <Grid
          container
          justify="center"
          direction="column"
          style={{ marginBottom: "1rem" }}
        >
          <Typography
            align="center"
            variant="h4"
            style={{ marginBottom: "2rem", color: "#666" }}
          >
            {headerData.categoryName}
          </Typography>
          <Typography
            align="center"
            component="p"
            style={{ color: "#666", lineHeight: "1.6" }}
          >
            {headerData.categoryDesciption}
          </Typography>
        </Grid>
        {solutions.map((solution, index) => {
          const data = solution.iconImage.data;
          console.log(
            solution.solutionName === this.state.solution.solutionName
          );
          return (
            <React.Fragment>
              <Paper
                elevation={3}
                // style={Styles}
                spacing={1}
                style={
                  solution.solutionName === this.state.solution.solutionName
                    ? Styles_After
                    : Styles
                }
                onClick={(e) => this.visibility(e, solution, index)}
              >
                <img
                  src={`data:image/jpeg;base64,${data}`}
                  alt="description of image"
                  width={45}
                  height={45}
                  style={{ paddingTop: 20 }}
                />
                <div
                  style={{
                    color: "#004d40",
                    marginTop: 15,
                    marginBottom: 8,
                    padding: 8,
                  }}
                >
                  {solution.solutionName}
                </div>
                <Divider />
                <Typography
                  style={{ padding: 8 }}
                  variant="caption"
                  display="block"
                  gutterBottom
                >
                  {solution.solutionShortDescription}
                </Typography>
              </Paper>
              {(index + 1) % 4 === 0 || solutions.length === index + 1 ? (
                <div id={"solution" + "-" + (index + 1)}></div>
              ) : (
                ""
              )}
            </React.Fragment>
          );
        })}
        {/* <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleClose}
          role={undefined}
          transition
          disablePortal
        >
          <Paper>
            <MenuList
              autoFocusItem={open}
              id="menu-list-grow"
              // onKeyDown={handleListKeyDown}
            >
              <MenuItem onClick={this.handleClose}>
                <SolutionDetails solution={solution} />
              </MenuItem>
            </MenuList>
          </Paper>
        </Popover> */}
      </div>
    );
  }
}
