import React from "react";
import BreadCrumb from "./BreadCrumb";
import Grid from "@material-ui/core/Grid";

export default function Header(props) {
  const { breadcrumb } = props;
  return (
    <Grid
      container
      spacing={2}
      justify="left"
      style={{ width: "100%", padding: 12 }}
    >
      <Grid item xs={5}>
        <img
          src={require("../assets/images/lti_mosaic_marketplace.svg")}
          style={{
            paddingLeft: "8px",
            alignItems: "center",
          }}
          width={200}
        />
      </Grid>
      <Grid item>
        <BreadCrumb breadcrumb={breadcrumb} />
      </Grid>
    </Grid>
  );
}
